package pl.gessel.arbitrationwall2

import android.app.Activity
import android.app.Service
import android.content.Context
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import com.crashlytics.android.Crashlytics
import com.google.firebase.FirebaseApp
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.HasServiceInjector
import io.fabric.sdk.android.Fabric
import io.realm.Realm
import io.realm.RealmConfiguration
import pl.gessel.arbitrationwall2.injection.DaggerApplicationComponent
import timber.log.Timber
import timber.log.Timber.DebugTree
import javax.inject.Inject


class GesselApplication : MultiDexApplication(), HasActivityInjector, HasServiceInjector {

    @Inject
    lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var dispatchingServiceInjector: DispatchingAndroidInjector<Service>

    override fun onCreate() {
        super.onCreate()
        DaggerApplicationComponent.builder().create(this).build().inject(this)
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }
        Fabric.with(this, Crashlytics())
        Realm.init(this)
        setRealmConfiguration()
        FirebaseApp.initializeApp(this)
    }

    private fun setRealmConfiguration() {
        val realmConfiguration = RealmConfiguration.Builder()
                .schemaVersion(4L)
                .deleteRealmIfMigrationNeeded()
                .build()
        Realm.setDefaultConfiguration(realmConfiguration)

    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> = dispatchingActivityInjector

    override fun serviceInjector(): AndroidInjector<Service> = dispatchingServiceInjector
}
