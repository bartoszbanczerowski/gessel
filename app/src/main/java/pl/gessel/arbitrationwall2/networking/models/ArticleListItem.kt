package pl.gessel.arbitrationwall2.networking.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
data class ArticleListItem constructor(@Json(name = "id")
                                       val articleId: Int = 0,
                                       @Json(name = "date_update")
                                       val modifiedDate: Date = Date(),
                                       @Json(name = "date")
                                       val createDate: Date = Date())