package pl.gessel.arbitrationwall2.networking

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import java.text.SimpleDateFormat
import java.util.*

class DateAdapter : JsonAdapter<Date>() {

    override fun fromJson(reader: JsonReader?): Date? {
        return sdf.parse(reader?.nextString())
    }

    override fun toJson(writer: JsonWriter?, value: Date?) {
        writer?.value(sdf.format(value))
    }

    companion object {
        val sdf = SimpleDateFormat("dd-MM-yyyy HH:mm")
    }
}