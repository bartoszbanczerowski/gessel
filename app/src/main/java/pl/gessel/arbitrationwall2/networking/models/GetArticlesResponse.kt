package pl.gessel.arbitrationwall2.networking.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GetArticlesResponse constructor(@Json(name = "ids")
                                           val articles: List<ArticleListItem>)