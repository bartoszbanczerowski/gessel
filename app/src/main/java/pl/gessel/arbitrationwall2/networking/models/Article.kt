package pl.gessel.arbitrationwall2.networking.models

import com.squareup.moshi.Json
import java.io.Serializable
import java.util.*

data class Article constructor(@Json(name = "article")
                               var articleId: Int = 0,
                               @Json(name = "category")
                               val category: Category,
                               @Json(name = "date_update")
                               val modifiedDate: Date = Date(),
                               @Json(name = "date")
                               val addedDate: Date = Date(),
                               @Json(name = "pdf")
                               val pdfUrl: String = "",
                               @Json(name = "title")
                               val title: String = "",
                               @Json(name = "sections")
                               val sections: List<ApiSections> = emptyList()) : Serializable