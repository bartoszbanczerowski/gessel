package pl.gessel.arbitrationwall2.networking.models


data class ApiSectionsModel constructor(var type: String = "",
                                       var text: String = "",
                                       var link: String = "",
                                       var articleId: Int = 0,
                                       var isBookmarked: Boolean = false)
