package pl.gessel.arbitrationwall2.networking.models

import com.squareup.moshi.Json

data class ApiSections constructor(@Json(name = "type")
                                   var type: String? = "",
                                   @Json(name = "text")
                                   var text: String? = "",
                                   @Json(name = "link")
                                   var link: String? = "")