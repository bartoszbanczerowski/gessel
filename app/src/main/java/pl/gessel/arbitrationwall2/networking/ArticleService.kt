package pl.gessel.arbitrationwall2.networking

import io.reactivex.Single
import okhttp3.ResponseBody
import pl.gessel.arbitrationwall2.networking.models.GetArticleResponse
import pl.gessel.arbitrationwall2.networking.models.GetArticlesResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ArticleService {

    @GET("api.php")
    fun getArticles(@Query("key") key: String,
                    @Query("action") action: String): Single<Response<GetArticlesResponse>>

    @GET("api.php")
    fun getArticle(@Query("key") key: String,
                   @Query("action") action: String,
                   @Query("id") id: Int): Single<Response<GetArticleResponse>>

    @GET("api.php")
    fun registerFcmToken(
            @Query("key") key: String,
            @Query("action") action: String,
            @Query("deviceToken") deviceToken: String?,
            @Query("deviceId") deviceId: String?,
            @Query("deviceType") deviceType: Int,
            @Query("status") status: Int): Single<Response<ResponseBody>>

    companion object {
        const val GESSEL_APPLICATION: String = "gessel-app"
        const val POST_IDS_ACTION: String = "postIds"
        const val GET_POST_ACTION: String = "getPost"
        const val REGISTER_DEVICE_ACTION = "registerDevice"
        const val DEVICE_TYPE = 1
    }
}