package pl.gessel.arbitrationwall2.networking.models

import com.squareup.moshi.Json

data class Category constructor(@Json(name = "id")
                                val id: Int,
                                @Json(name = "name")
                                val name: String)