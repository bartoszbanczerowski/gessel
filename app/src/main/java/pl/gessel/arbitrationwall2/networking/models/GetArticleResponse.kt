package pl.gessel.arbitrationwall2.networking.models

import com.squareup.moshi.Json

open class GetArticleResponse constructor(@Json(name = "post")
                                          val article: Article)