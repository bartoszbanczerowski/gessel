package pl.gessel.arbitrationwall2.realm

import io.realm.RealmList
import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import java.util.*

@RealmClass
open class ArticleRealm constructor(@PrimaryKey
                                    var articleId: Int = 0,
                                    var category: CategoryRealm? = null,
                                    var modifiedDate: Date = Date(),
                                    var addedDate: Date = Date(),
                                    var pdfUrl: String = "",
                                    var title: String = "",
                                    var isBookmarked: Boolean = false,
                                    var sections: RealmList<ApiSectionRealm> = RealmList()
) : RealmModel