package pl.gessel.arbitrationwall2.realm

import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class CategoryRealm constructor(@PrimaryKey
                                     var id: Int = 0,
                                     var name: String = "") : RealmModel