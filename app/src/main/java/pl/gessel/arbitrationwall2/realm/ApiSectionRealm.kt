package pl.gessel.arbitrationwall2.realm

import io.realm.RealmModel
import io.realm.annotations.RealmClass

@RealmClass
open class ApiSectionRealm constructor(var type: String = ApiSectionTypeRealm.UNKNOWN.toString(),
                                       var text: String = "",
                                       var link: String = "",
                                       var articleId: Int = 0,
                                       var isBookmarked: Boolean = false) : RealmModel