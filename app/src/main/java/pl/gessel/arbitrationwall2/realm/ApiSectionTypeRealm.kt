package pl.gessel.arbitrationwall2.realm

enum class ApiSectionTypeRealm(var type: String) {
    TEXT("p"),
    TABLE("table"),
    LIST_OL("ol"),
    LIST_UL("ul"),
    LINK("a"),
    MAIL("mail"),
    HEADER("h1"),
    SUB_HEADER("h2"),
    SUB_SUB_HEADER("h3"),
    SUB_SUB_SUB_HEADER("h4"),
    SUB_SUB_SUB_SUB_HEADER("h5"),
    IMAGE("img"),
    FULL_IMAGE("fullimageCELL"),
    UNKNOWN("unknown"),
    TEXT_BOLD("b");
}