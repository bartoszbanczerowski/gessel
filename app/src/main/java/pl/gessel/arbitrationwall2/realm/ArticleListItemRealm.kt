package pl.gessel.arbitrationwall2.realm

import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import java.util.*

@RealmClass
open class ArticleListItemRealm constructor(@PrimaryKey
                                            var articleId: Int = 0,
                                            var modifiedDate: Date = Date(),
                                            var createDate: Date = Date()) : RealmModel
