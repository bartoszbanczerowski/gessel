package pl.gessel.arbitrationwall2.injection

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.gessel.arbitrationwall2.desktop.DesktopActivity
import pl.gessel.arbitrationwall2.fcm.FCMTokenGenerator
import pl.gessel.arbitrationwall2.welcome.WelcomeActivity


@Module
abstract class ActivityModuleBuilder {

    @ContributesAndroidInjector(modules = [WelcomeModule::class])
    abstract fun bindWelcomeActivity(): WelcomeActivity

    @ContributesAndroidInjector(modules = [DesktopModule::class])
    abstract fun bindDesktopActivity(): DesktopActivity

    @ContributesAndroidInjector
    abstract fun bindFcmTokenGenerator(): FCMTokenGenerator
}