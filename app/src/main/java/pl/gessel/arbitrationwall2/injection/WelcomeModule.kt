package pl.gessel.arbitrationwall2.injection

import android.content.SharedPreferences
import android.net.ConnectivityManager
import dagger.Module
import dagger.Provides
import pl.gessel.arbitrationwall2.core.SchedulerProvider
import pl.gessel.arbitrationwall2.networking.ArticleService
import pl.gessel.arbitrationwall2.welcome.WelcomePresenterImpl


@Module
class WelcomeModule {

    @Provides
    fun provideWelcomePresenter(
            sharedPreferences: SharedPreferences,
            connectivityManager: ConnectivityManager,
            schedulerProvider: SchedulerProvider,
            articleService: ArticleService):
            WelcomePresenterImpl = WelcomePresenterImpl(sharedPreferences, connectivityManager, schedulerProvider,
            articleService)
}