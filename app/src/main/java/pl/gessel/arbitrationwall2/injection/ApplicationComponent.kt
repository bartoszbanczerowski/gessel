package pl.gessel.arbitrationwall2.injection

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import pl.gessel.arbitrationwall2.GesselApplication
import javax.inject.Singleton


@Singleton
@Component(
        modules = [
            AndroidSupportInjectionModule::class,
            ApplicationModule::class,
            NetworkingModule::class,
            ActivityModuleBuilder::class,
            FragmentModuleBuilder::class
        ]
)
interface ApplicationComponent : AndroidInjector<GesselApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun create(application: GesselApplication): Builder

        fun build(): ApplicationComponent
        fun networkingModule(networkingModule: NetworkingModule): Builder
    }
}