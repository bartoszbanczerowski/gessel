package pl.gessel.arbitrationwall2.injection

import dagger.Module
import dagger.Provides
import pl.gessel.arbitrationwall2.desktop.search.SearchPresenterImpl

@Module
class SearchModule{

    @Provides
    fun providesSearchPresenter() = SearchPresenterImpl()
}