package pl.gessel.arbitrationwall2.injection

import dagger.Module
import dagger.Provides
import dagger.Reusable
import pl.gessel.arbitrationwall2.R
import pl.gessel.arbitrationwall2.desktop.arbitrationlibrary.ArbitrationCategoryAdapter
import pl.gessel.arbitrationwall2.desktop.arbitrationlibrary.ArbitrationLibraryArticlePresenterImpl
import pl.gessel.arbitrationwall2.desktop.arbitrationlibrary.ArbitrationLibraryPresenterImpl
import pl.gessel.arbitrationwall2.desktop.arbitrationlibrary.CategoryItem

@Module
class ArbitrationLibraryModule {


    @Provides
    @Reusable
    fun providesCategoryItems(): MutableList<CategoryItem> {
        return mutableListOf(
                CategoryItem(R.string.arbitration_library_arbitration_rules,
                        ARBITRATION_RULES_ID, 0),
                CategoryItem(R.string.arbitration_library_arbitration_laws,
                        ARBITRATION_LAWS_ID, 0),
                CategoryItem(R.string.arbitration_library_arbitration_fees,
                        ARBITRATION_FEES_ID, 0),
                CategoryItem(R.string.arbitration_library_international_instruments,
                        INTERNATIONAL_INSTRUMENTS_ID, 0),
                CategoryItem(R.string.arbitration_library_arbitration_institutions,
                        ARBITRATION_INSTITUTIONS_ID, 0),
                CategoryItem(R.string.arbitration_library_sample_clauses,
                        SAMPLE_CLAUSES_ID, 0),
                CategoryItem(R.string.arbitration_library_current_news,
                        CURRENT_NEWS_ID, 0))
    }

    @Provides
    fun providesArbitrationPresenter(categories: List<CategoryItem>) = ArbitrationLibraryPresenterImpl(categories)

    @Provides
    fun providesArbitrationArticlesPresenter() = ArbitrationLibraryArticlePresenterImpl()

    @Provides
    fun providesArbitrationCategoryAdapter(categories: List<CategoryItem>) = ArbitrationCategoryAdapter(categories)

    companion object {
        const val ARBITRATION_RULES_ID = 343
        const val ARBITRATION_LAWS_ID = 342
        const val ARBITRATION_FEES_ID = 340
        const val INTERNATIONAL_INSTRUMENTS_ID = 345
        const val ARBITRATION_INSTITUTIONS_ID = 341
        const val SAMPLE_CLAUSES_ID = 346
        const val CURRENT_NEWS_ID = 344
    }
}