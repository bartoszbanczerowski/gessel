package pl.gessel.arbitrationwall2.injection

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.gessel.arbitrationwall2.desktop.DesktopFragment
import pl.gessel.arbitrationwall2.desktop.arbitrationlibrary.ArbitrationLibraryArticleFragment
import pl.gessel.arbitrationwall2.desktop.arbitrationlibrary.ArbitrationLibraryFragment
import pl.gessel.arbitrationwall2.desktop.arbitrationlibrary.ArticleDetailFragment
import pl.gessel.arbitrationwall2.desktop.bookmarks.BookmarksFragment
import pl.gessel.arbitrationwall2.desktop.search.SearchFragment
import pl.gessel.arbitrationwall2.desktop.statics.StaticFragment

@Module
abstract class FragmentModuleBuilder {

    @ContributesAndroidInjector(modules = [DesktopModule::class])
    abstract fun bindDesktopFragment(): DesktopFragment

    @ContributesAndroidInjector(modules = [StaticFragmentModule::class])
    abstract fun bindAboutStaticFragment(): StaticFragment

    @ContributesAndroidInjector(modules = [ArbitrationLibraryModule::class])
    abstract fun bindArbitrationLibraryFragment(): ArbitrationLibraryFragment

    @ContributesAndroidInjector(modules = [ArbitrationLibraryModule::class])
    abstract fun bindArbitrationLibraryArticleFragment(): ArbitrationLibraryArticleFragment

    @ContributesAndroidInjector(modules = [ArticleDetailModule::class])
    abstract fun bindArticleDetailFragment(): ArticleDetailFragment

    @ContributesAndroidInjector(modules = [BookmarksModule::class])
    abstract fun bindBookmarksFragment(): BookmarksFragment

    @ContributesAndroidInjector(modules = [SearchModule::class])
    abstract fun bindSearchFragment(): SearchFragment

}