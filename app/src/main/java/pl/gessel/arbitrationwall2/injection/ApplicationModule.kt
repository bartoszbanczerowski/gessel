package pl.gessel.arbitrationwall2.injection

import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.preference.PreferenceManager
import dagger.Module
import dagger.Provides
import dagger.Reusable
import pl.gessel.arbitrationwall2.GesselApplication
import pl.gessel.arbitrationwall2.core.BaseSchedulerProvider
import pl.gessel.arbitrationwall2.core.SchedulerProvider
import javax.inject.Singleton


@Module
class ApplicationModule {

    @Provides
    @Singleton
    fun providesSharedPreferences(application: GesselApplication): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(application)

    @Provides
    @Reusable
    fun providesBaseSchedulerProvider(): BaseSchedulerProvider = SchedulerProvider()

    @Provides
    @Singleton
    fun providesConnectivityManager(application: GesselApplication): ConnectivityManager =
            application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    @Singleton
    @Provides
    fun provideContext(application: GesselApplication): Context {
        return application
    }
}
