package pl.gessel.arbitrationwall2.injection

import android.content.Context
import dagger.Module
import dagger.Provides
import pl.gessel.arbitrationwall2.desktop.arbitrationlibrary.ArticleDetailPresenterImpl

@Module
class ArticleDetailModule{

    @Provides
    fun providePresenter(context: Context) = ArticleDetailPresenterImpl(context)
}