package pl.gessel.arbitrationwall2.injection

import dagger.Module
import dagger.Provides
import pl.gessel.arbitrationwall2.desktop.statics.StaticPresenterImpl

@Module
class StaticFragmentModule {

    @Provides
    fun providesStaticPresenter() = StaticPresenterImpl()
}