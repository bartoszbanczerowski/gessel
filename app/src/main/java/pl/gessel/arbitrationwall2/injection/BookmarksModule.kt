package pl.gessel.arbitrationwall2.injection

import dagger.Module
import dagger.Provides
import pl.gessel.arbitrationwall2.desktop.bookmarks.BookmarksPresenterImpl

@Module
class BookmarksModule {

    @Provides
    fun providesBookmarsPresenter() = BookmarksPresenterImpl()
}