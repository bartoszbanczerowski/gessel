package pl.gessel.arbitrationwall2.injection

import android.support.v7.widget.GridLayoutManager
import dagger.Module
import dagger.Provides
import dagger.Reusable
import pl.gessel.arbitrationwall2.GesselApplication
import pl.gessel.arbitrationwall2.R
import pl.gessel.arbitrationwall2.desktop.DesktopAdapter
import pl.gessel.arbitrationwall2.desktop.DesktopItem
import pl.gessel.arbitrationwall2.desktop.DesktopItemType

@Module
class DesktopModule {

    @Provides
    @Reusable
    fun providesDesktopItems(): MutableList<DesktopItem> {
        return mutableListOf(
                DesktopItem(R.string.desktop_title_arbitration_library,
                        R.drawable.arbitration_library,
                        DesktopItemType.ARBITRATION_LIBRARY),
                DesktopItem(R.string.desktop_title_about_the_wall,
                        R.drawable.about_the_wall,
                        DesktopItemType.ABOUT_THE_WALL),
                DesktopItem(R.string.desktop_title_users_manual,
                        R.drawable.users_manual,
                        DesktopItemType.USERS_MANUAL),
                DesktopItem(R.string.desktop_title_about_gessel,
                        R.drawable.about_gessel,
                        DesktopItemType.ABOUT_GESSEL),
                DesktopItem(R.string.desktop_title_bookmarks,
                        R.drawable.bookmarks,
                        DesktopItemType.BOOKMARKS),
                DesktopItem(R.string.desktop_title_settings,
                        R.drawable.settings,
                        DesktopItemType.SETTINGS))
    }

    @Provides
    @Reusable
    fun providesDesktopAdapter(desktopItems: List<DesktopItem>): DesktopAdapter = DesktopAdapter(desktopItems)

    @Provides
    fun providesGridLayoutManager(application: GesselApplication): GridLayoutManager = GridLayoutManager(application, 2)
}