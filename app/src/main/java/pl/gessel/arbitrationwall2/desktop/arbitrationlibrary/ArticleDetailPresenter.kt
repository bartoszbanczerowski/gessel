package pl.gessel.arbitrationwall2.desktop.arbitrationlibrary

import pl.gessel.arbitrationwall2.core.BasePresenter
import pl.gessel.arbitrationwall2.networking.models.ApiSectionsModel
import pl.gessel.arbitrationwall2.realm.ApiSectionRealm
import pl.gessel.arbitrationwall2.realm.ArticleRealm

interface ArticleDetailPresenter : BasePresenter<ArticleDetailPresenter.ArticleDetaiView> {

    fun findArticle(articleId: Int?): ArticleRealm?
    fun onBookmarkClicked() {}
    fun onPdfClicked() {}
    fun onMenuClicked(isDrawerOpen: Boolean) {}
    fun isPdfAvailable(): Boolean {
        return true
    }

    fun onApiSectionClicked(apiSectionPosition: Int)
    fun getNavigationDrawerHeaders(): List<ApiSectionRealm> {
        return emptyList()
    }

    fun isNavigationDrawerHeader(apiSectionRealm: ApiSectionsModel): Boolean {
        return false
    }

    interface ArticleDetaiView {
        fun openPdf(url: String?)
        fun bookmarkChecked(isChecked: Boolean, sectionPosition: Int) {}
        fun menuOpen() {}
        fun menuClosed() {}
        fun showSnackBarMessage(message: String) {}
    }
}
