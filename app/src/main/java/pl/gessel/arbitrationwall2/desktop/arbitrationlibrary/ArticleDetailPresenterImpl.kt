package pl.gessel.arbitrationwall2.desktop.arbitrationlibrary

import android.content.Context
import io.realm.Realm
import io.realm.RealmList
import pl.gessel.arbitrationwall2.R
import pl.gessel.arbitrationwall2.networking.models.ApiSections
import pl.gessel.arbitrationwall2.networking.models.ApiSectionsModel
import pl.gessel.arbitrationwall2.realm.ApiSectionRealm
import pl.gessel.arbitrationwall2.realm.ApiSectionTypeRealm
import pl.gessel.arbitrationwall2.realm.ArticleRealm
import javax.inject.Inject

class ArticleDetailPresenterImpl @Inject constructor(val context: Context) : ArticleDetailPresenter {

    companion object {
        const val DEFAULT_EMPTY_BODY = ""
        const val EMPTY_BODY = "  "
        const val DOUBLE_EMPTY_BODY = "   "
        const val TRIPLE_EMPTY_BODY = "    "
        const val FOURTH_EMPTY_BODY = "     "
    }

    private lateinit var articleDetailView: ArticleDetailPresenter.ArticleDetaiView
    private var realm: Realm? = null
    private var article: ArticleRealm? = null
    private var isBookmarkEnabled: Boolean = false

    override fun init(view: ArticleDetailPresenter.ArticleDetaiView) {
        articleDetailView = view
        realm = Realm.getDefaultInstance()
    }

    override fun findArticle(articleId: Int?): ArticleRealm? {
        article = realm?.where(ArticleRealm::class.java)
                ?.equalTo("articleId", articleId)?.findFirst()
        return article
    }

    override fun onBookmarkClicked() {
        isBookmarkEnabled = !isBookmarkEnabled
        val message = if (isBookmarkEnabled) context.getString(R.string.article_detail_bookmark_enabled)
        else context.getString(R.string.article_detail_bookmark_disabled)
        articleDetailView.showSnackBarMessage(message)
    }

    override fun onPdfClicked() {
        articleDetailView.openPdf(article?.pdfUrl)
    }

    override fun onMenuClicked(isDrawerOpen: Boolean) {
        if (isDrawerOpen) articleDetailView.menuClosed()
        else articleDetailView.menuOpen()
    }

    override fun isPdfAvailable(): Boolean {
        return !article?.pdfUrl.isNullOrEmpty()
    }

    override fun onApiSectionClicked(apiSectionPosition: Int) {
        if (isBookmarkEnabled) {
            realm?.executeTransaction { realm ->
                article?.sections?.get(apiSectionPosition)
                        ?.let { apiSectionRealm ->
                            apiSectionRealm.isBookmarked = !apiSectionRealm.isBookmarked
                        }
                realm.copyToRealmOrUpdate(article)
            }
            articleDetailView.bookmarkChecked(article?.sections?.get(apiSectionPosition)!!.isBookmarked,
                    apiSectionPosition)
        }
    }

    override fun getNavigationDrawerHeaders(): List<ApiSectionRealm> {
        return article?.sections?.filter { it ->
            it.type == ApiSectionTypeRealm.HEADER.type
                    || it.type == ApiSectionTypeRealm.SUB_HEADER.type
                    || it.type == ApiSectionTypeRealm.SUB_SUB_HEADER.type
                    || it.type == ApiSectionTypeRealm.SUB_SUB_SUB_HEADER.type
                    || it.type == ApiSectionTypeRealm.SUB_SUB_SUB_SUB_HEADER.type
        }.orEmpty().toList()
    }

    override fun isNavigationDrawerHeader(apiSectionRealm: ApiSectionsModel): Boolean {
        return apiSectionRealm.type == ApiSectionTypeRealm.HEADER.type
                || apiSectionRealm.type == ApiSectionTypeRealm.SUB_HEADER.type
                || apiSectionRealm.type == ApiSectionTypeRealm.SUB_SUB_HEADER.type
                || apiSectionRealm.type == ApiSectionTypeRealm.SUB_SUB_SUB_HEADER.type
                || apiSectionRealm.type == ApiSectionTypeRealm.SUB_SUB_SUB_SUB_HEADER.type
    }

    fun getSubHeaderPrefix(apiSectionRealm: ApiSectionRealm): String {
        if (apiSectionRealm.type == ApiSectionTypeRealm.SUB_HEADER.type) return EMPTY_BODY + EMPTY_BODY
        if (apiSectionRealm.type == ApiSectionTypeRealm.SUB_SUB_HEADER.type) return DOUBLE_EMPTY_BODY
        if (apiSectionRealm.type == ApiSectionTypeRealm.SUB_SUB_SUB_HEADER.type) return TRIPLE_EMPTY_BODY
        if (apiSectionRealm.type == ApiSectionTypeRealm.SUB_SUB_SUB_SUB_HEADER.type) return FOURTH_EMPTY_BODY
        return DEFAULT_EMPTY_BODY
    }

    fun mapApiSections(apiSections: RealmList<ApiSectionRealm>): List<ApiSectionsModel> {
        val sections = ArrayList<ApiSectionsModel>()
        apiSections.forEach { apiSection:  ApiSectionRealm->
            sections.add(
                ApiSectionsModel(
                text = apiSection.text,
                type = apiSection.type,
                link = apiSection.link,
                articleId = apiSection.articleId,
                isBookmarked = apiSection.isBookmarked)
            )
        }
        return sections
    }

}
