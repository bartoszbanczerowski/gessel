package pl.gessel.arbitrationwall2.desktop.arbitrationlibrary

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v4.widget.DrawerLayout
import android.text.Html
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.SubMenu
import android.view.View
import android.view.ViewGroup
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.bookmark_view.view.*
import kotlinx.android.synthetic.main.fragment_article_detail.*
import kotlinx.android.synthetic.main.fragment_article_detail.view.*
import pl.gessel.arbitrationwall2.R
import pl.gessel.arbitrationwall2.core.BaseFragment
import pl.gessel.arbitrationwall2.core.BookmarkView
import pl.gessel.arbitrationwall2.core.SectionCreator
import pl.gessel.arbitrationwall2.realm.ApiSectionRealm
import pl.gessel.arbitrationwall2.realm.ArticleRealm
import javax.inject.Inject

class ArticleDetailFragment : BaseFragment(), ArticleDetailPresenter.ArticleDetaiView {


    @Inject
    lateinit var presenter: ArticleDetailPresenterImpl

    private val sectionCreator: SectionCreator = SectionCreator()
    private val headersPositions = ArrayList<View>()
    lateinit var rootView: View
    var article: ArticleRealm? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_article_detail, container, false)
        val articleId = arguments?.getInt(ARTICLE_ID)
        val bookmarkId = arguments?.getInt(BOOKMARK_ID)

        presenter.init(this)
        article = presenter.findArticle(articleId)

        rootView.articleCategoryTitle.text = article?.category?.name
        rootView.articleTitle.text = Html.fromHtml(article?.title)
        rootView.articleItemBookmark.setOnClickListener { v -> presenter.onBookmarkClicked() }
        rootView.articleItemMenu.setOnClickListener { v -> presenter.onMenuClicked(rootView.drawer_layout.isDrawerOpen(Gravity.START)) }
        rootView.articleItemPdf.visibility =
            if (presenter.isPdfAvailable()) View.VISIBLE else View.INVISIBLE
        rootView.articleItemPdf.setOnClickListener { v -> presenter.onPdfClicked() }
        createMenuItems(rootView)

        toolbarManager.setToolbarTitle(R.string.desktop_title_arbitration_articles)
        toolbarManager.showToolbarBackArrow(true)

        return rootView
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onStart() {
        super.onStart()

        val mapApiSections = presenter.mapApiSections(article!!.sections)
        Handler().postDelayed( {
            mapApiSections.forEachIndexed { position, apiSectionRealm ->
                val createSection = sectionCreator.createSection(rootView.context, apiSectionRealm)
                createSection.setOnClickListener { v -> presenter.onApiSectionClicked(position) }
                rootView.sectionDetailLinearLayout.addView(createSection)
                if (presenter.isNavigationDrawerHeader(apiSectionRealm)) {
                    headersPositions.add(createSection)
                }
            }
            rootView.synchronizationProgressBar.visibility = View.GONE
        },600)
    }

    override fun openPdf(url: String?) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(browserIntent)
    }

    override fun bookmarkChecked(isChecked: Boolean, sectionPosition: Int) {
        val view = view!!.sectionDetailLinearLayout.getChildAt(sectionPosition)

        if (view is BookmarkView && isChecked) {
            view.bookmarkImage.visibility = View.VISIBLE
        } else if (view is BookmarkView && !isChecked) {
            view.bookmarkImage.visibility = View.INVISIBLE
        }
    }

    override fun menuOpen() {
        drawer_layout.openDrawer(Gravity.START)
    }

    override fun menuClosed() {
        drawer_layout.closeDrawer(Gravity.START)
    }

    private fun createMenuItems(rootView: View) {
        val mainHeaders = LinkedHashMap<SubMenu, Int>()
        val navigationDrawerHeaders = presenter.getNavigationDrawerHeaders()
        var isEmptyItemAdded = false

        navigationDrawerHeaders.forEachIndexed { position: Int, apiSectionRealm: ApiSectionRealm ->
            rootView.nav_view.menu.add(
                0, position, position,
                presenter.getSubHeaderPrefix(apiSectionRealm) + apiSectionRealm.text
            )
        }

        setMenuVisibility(navigationDrawerHeaders, rootView)
        setNavigationItemListener(rootView)
    }

    private fun setMenuVisibility(navigationDrawerHeaders: List<ApiSectionRealm>, rootView: View) {
        if (navigationDrawerHeaders.isEmpty()) {
            rootView.drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            rootView.articleItemMenu.visibility = View.INVISIBLE
        }
    }

    private fun setNavigationItemListener(rootView: View) {
        rootView.nav_view.setNavigationItemSelectedListener { item: MenuItem ->
            val view = headersPositions[item.itemId]
            rootView.scrollView.scrollTo(0, view.top)
            true
        }
    }

    override fun showSnackBarMessage(message: String) {
        Snackbar.make(view!!, message, Snackbar.LENGTH_LONG).show()
    }

    companion object {
        const val ARTICLE_ID = "articleID"
        const val BOOKMARK_ID = "articleID"
        @JvmStatic
        fun newInstance(articleId: Int) =
            ArticleDetailFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARTICLE_ID, articleId)
                }
            }

        @JvmStatic
        fun newInstance(articleId: Int, bookmarkId: Int) =
            ArticleDetailFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARTICLE_ID, articleId)
                    putInt(BOOKMARK_ID, bookmarkId)
                }
            }
    }

}
