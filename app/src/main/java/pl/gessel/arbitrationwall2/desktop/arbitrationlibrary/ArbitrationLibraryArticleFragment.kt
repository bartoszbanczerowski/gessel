package pl.gessel.arbitrationwall2.desktop.arbitrationlibrary

import android.content.Context
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.AndroidSupportInjection
import io.realm.RealmResults
import kotlinx.android.synthetic.main.fragment_arbitration_library_articles.view.*
import pl.gessel.arbitrationwall2.R
import pl.gessel.arbitrationwall2.core.BaseFragment
import pl.gessel.arbitrationwall2.realm.ArticleRealm
import javax.inject.Inject

class ArbitrationLibraryArticleFragment : BaseFragment(), ArbitrationLibraryPresenter.ArbitrationLibraryView {

    @Inject
    lateinit var presenter: ArbitrationLibraryArticlePresenterImpl

    @Inject
    lateinit var arbitrationArticleAdapter: ArbitrationArticleAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_arbitration_library_articles, container, false)
        val categoryId = arguments?.getInt(CATEGORY_ID) ?: 0
        val categoName = arguments?.getInt(CATEGORY_NAME) ?: 0

        rootView.arbitrationArticles.text = getString(categoName)

        presenter.init(this)
        presenter.getArticlesForCategory(categoryId)
        arbitrationArticleAdapter.presenter = presenter
        rootView.articles.adapter = arbitrationArticleAdapter
        rootView.articles.layoutManager = LinearLayoutManager(rootView.context)

        rootView.arbitrationOrderByName.setOnClickListener { v -> presenter.sortArticlesByName() }
        rootView.arbitrationOrderByDate.setOnClickListener { v -> presenter.sortArticlesByDate() }
        toolbarManager.setToolbarTitle(R.string.desktop_title_arbitration_articles)
        toolbarManager.showToolbarBackArrow(true)
        return rootView
    }


    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun updateArticles(articles: RealmResults<ArticleRealm>?) {
        arbitrationArticleAdapter.updateArticles(articles)
    }

    override fun goToArticle(articleId: Int) {
        val fragment = ArticleDetailFragment.newInstance(articleId)
        activity!!.supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.desktopContainer, fragment, "Articles")
                .addToBackStack("Articles")
                .commit()
    }

    companion object {
        const val CATEGORY_ID = "categoryId"
        const val CATEGORY_NAME = "categoryName"
        @JvmStatic
        fun newInstance(categoryId: Int, @StringRes categoryName: Int) =
                ArbitrationLibraryArticleFragment().apply {
                    arguments = Bundle().apply {
                        putInt(CATEGORY_ID, categoryId)
                        putInt(CATEGORY_NAME, categoryName)
                    }
                }
    }

}
