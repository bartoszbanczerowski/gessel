package pl.gessel.arbitrationwall2.desktop.arbitrationlibrary

import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import io.realm.RealmResults
import kotlinx.android.synthetic.main.article_item.view.*
import org.joda.time.LocalDate
import pl.gessel.arbitrationwall2.R
import pl.gessel.arbitrationwall2.injection.ArbitrationLibraryModule
import pl.gessel.arbitrationwall2.realm.ArticleRealm
import java.util.*
import javax.inject.Inject

class ArbitrationArticleAdapter @Inject constructor()
    : RecyclerView.Adapter<ArbitrationArticleAdapter.ArbitrationViewHolder>() {

    lateinit var presenter: ArbitrationLibraryArticlePresenterImpl
    private var articles: RealmResults<ArticleRealm>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArbitrationViewHolder {
        return ArbitrationViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.article_item, parent, false))
    }

    override fun getItemCount(): Int {
        return articles?.size ?: 0
    }

    override fun onBindViewHolder(holder: ArbitrationViewHolder, position: Int) {
        holder.articleTitle.text = Html.fromHtml(articles?.get(position)?.title)
        holder.articleDate.text = LocalDate.fromDateFields(articles?.get(position)?.addedDate).toString(DATE_PATTERN, Locale.getDefault())
        holder.articleDate.visibility = if (articles?.get(position)?.category?.id == ArbitrationLibraryModule.CURRENT_NEWS_ID)
            View.VISIBLE else View.GONE
        holder.articleBookmark.visibility =
                if (articles?.get(position)!!.isBookmarked) View.VISIBLE
                else View.INVISIBLE
        holder.view.setOnClickListener {
            presenter.onArticleClicked(articles!![position]!!.articleId)
        }
        holder.articleBookmark.visibility =
                if (isArticleBookmarked(articles?.get(position)!!)) View.VISIBLE else View.INVISIBLE
    }

    class ArbitrationViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        val articleTitle: TextView = view.articleItemTitle
        val articleDate: TextView = view.articleItemDate
        val articleBookmark: ImageView = view.articleItemBookmark
    }

    fun updateArticles(articles: RealmResults<ArticleRealm>?) {
        this.articles = articles
        notifyDataSetChanged()
    }

    private fun isArticleBookmarked(articleRealm: ArticleRealm): Boolean {
        val isArticleSectionBookmarked = articleRealm.sections.any { apiSectionRealm -> apiSectionRealm.isBookmarked }
        return articleRealm.isBookmarked || isArticleSectionBookmarked
    }

    companion object {
        private const val DATE_PATTERN = "dd MMM yyyy"
    }
}