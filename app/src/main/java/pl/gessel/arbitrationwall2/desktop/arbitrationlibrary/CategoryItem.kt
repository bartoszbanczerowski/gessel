package pl.gessel.arbitrationwall2.desktop.arbitrationlibrary

import android.support.annotation.StringRes

class CategoryItem constructor(@StringRes var title: Int,
                               var categoryId: Int,
                               var articlesSize: Int)