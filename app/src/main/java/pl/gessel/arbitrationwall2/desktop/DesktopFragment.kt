package pl.gessel.arbitrationwall2.desktop

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_desktop.view.*
import pl.gessel.arbitrationwall2.R
import pl.gessel.arbitrationwall2.core.BaseFragment
import pl.gessel.arbitrationwall2.desktop.arbitrationlibrary.ArbitrationLibraryFragment
import pl.gessel.arbitrationwall2.desktop.bookmarks.BookmarksFragment
import pl.gessel.arbitrationwall2.desktop.statics.StaticFragment
import javax.inject.Inject


class DesktopFragment : BaseFragment(), DesktopPresenter.DesktopView {

    @Inject
    lateinit var desktopPresenterImpl: DesktopPresenterImpl

    @Inject
    lateinit var desktopAdapter: DesktopAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_desktop, container, false)
        desktopPresenterImpl.init(this)
        desktopAdapter.desktopPresenter = desktopPresenterImpl
        toolbarManager.setToolbarTitle(R.string.desktop_title_arbitration_wall)
        toolbarManager.showToolbarBackArrow(false)

        view.desktop_arbitration_library.setOnClickListener { goToArbitrationWall() }
        view.desktop_about_gessel.setOnClickListener { goToAboutGessel() }
        view.desktop_about_the_wall.setOnClickListener { goToAboutTheWall() }
        view.desktop_bookmarks.setOnClickListener { goToBookmarks() }
        view.desktop_user_manual.setOnClickListener { goToUsersManual() }
        view.desktop_settings.setOnClickListener { goToSettings() }
        return view
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun goToAboutGessel() {
        val fragment = StaticFragment()
        val bundle = Bundle()
        bundle.putInt(StaticFragment.STATIC_FRAGMENT_ID, StaticFragment.ABOUT_GESSEL_ID)
        fragment.arguments = bundle
        activity!!.supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.desktopContainer, fragment, "AboutGessel")
                .addToBackStack("AboutGessel")
                .commit()
    }

    override fun goToUsersManual() {
        val fragment = StaticFragment()
        val bundle = Bundle()
        bundle.putInt(StaticFragment.STATIC_FRAGMENT_ID, StaticFragment.USERS_MANUAL_ID)
        fragment.arguments = bundle
        activity!!.supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.desktopContainer, fragment, "UserManual")
                .addToBackStack("UserManual")
                .commit()
    }

    override fun goToArbitrationWall() {
        val fragment = ArbitrationLibraryFragment()
        activity!!.supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.desktopContainer, fragment, "ArbitrationWall")
                .addToBackStack("ArbitrationWall")
                .commit()
    }

    override fun goToAboutTheWall() {
        val fragment = StaticFragment()
        val bundle = Bundle()
        bundle.putInt(StaticFragment.STATIC_FRAGMENT_ID, StaticFragment.ABOUT_THE_WALL_ID)
        fragment.arguments = bundle
        (activity as DesktopActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as DesktopActivity).supportActionBar?.setDisplayShowHomeEnabled(true)
        activity!!.supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.desktopContainer, fragment, "AboutTheWall")
                .addToBackStack("AboutTheWall")
                .commit()
    }

    override fun goToBookmarks() {
        val fragment = BookmarksFragment.newInstance()
        activity!!.supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.desktopContainer, fragment, "Bookmarks")
                .addToBackStack("Bookmarks")
                .commit()
    }

    override fun goToSettings() {
        val intent = Intent()
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            intent.action = "android.settings.APP_NOTIFICATION_SETTINGS"
            intent.putExtra("android.provider.extra.APP_PACKAGE", activity?.packageName)
        } else {
            intent.action = "android.settings.APP_NOTIFICATION_SETTINGS"
            intent.putExtra("app_package", activity?.packageName)
            intent.putExtra("app_uid", activity?.applicationInfo?.uid)
        }

        activity?.startActivity(intent)
    }
}
