package pl.gessel.arbitrationwall2.desktop.arbitrationlibrary

import io.realm.Realm
import pl.gessel.arbitrationwall2.realm.ArticleRealm
import javax.inject.Inject

class ArbitrationLibraryPresenterImpl @Inject constructor(val categories: List<CategoryItem>) :
        ArbitrationLibraryPresenter {

    lateinit var arbitrationView: ArbitrationLibraryPresenter.ArbitrationLibraryView
    lateinit var realm: Realm

    override fun init(view: ArbitrationLibraryPresenter.ArbitrationLibraryView) {
        arbitrationView = view
        realm = Realm.getDefaultInstance()
        createCategoryItems()
    }

    private fun createCategoryItems() {
        categories.forEach { categoryItem: CategoryItem ->
            val articles = realm.where(ArticleRealm::class.java).equalTo("category.id",
                    categoryItem.categoryId).findAll()
            categoryItem.articlesSize = articles.size
        }
        arbitrationView.updateCategories(categories)
    }

    override fun onCategoryClicked(categoryId: Int) {
        categories.find { categoryItem -> categoryItem.categoryId == categoryId }
                .let { categoryItem ->
                    arbitrationView.goToCategoryArticles(categoryItem!!.categoryId, categoryItem.title)
                }
    }
}