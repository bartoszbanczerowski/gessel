package pl.gessel.arbitrationwall2.desktop

import javax.inject.Inject

class DesktopPresenterImpl @Inject constructor() : DesktopPresenter {

    private lateinit var view: DesktopPresenter.DesktopView

    override fun init(view: DesktopPresenter.DesktopView) {
        this.view = view
    }

    override fun onDesktopItemClicked(position: Int) {
        when (position) {
            ARBITRATION_LIBRARY -> view.goToArbitrationWall()
            ABOUT_THE_WALL -> view.goToAboutTheWall()
            USERS_MANUAL -> view.goToUsersManual()
            ABOUT_GESSEL -> view.goToAboutGessel()
            BOOKMARKS -> view.goToBookmarks()
            SETTINGS -> view.goToSettings()
            else -> throw Exception("Unknown Desktop Item")
        }
    }

    companion object {
        private const val ARBITRATION_LIBRARY = 0
        private const val ABOUT_THE_WALL = 1
        private const val USERS_MANUAL = 2
        private const val ABOUT_GESSEL = 3
        private const val BOOKMARKS = 4
        private const val SETTINGS = 5
    }

}