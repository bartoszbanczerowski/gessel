package pl.gessel.arbitrationwall2.desktop.arbitrationlibrary

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.category_item.view.*
import pl.gessel.arbitrationwall2.R
import javax.inject.Inject

class ArbitrationCategoryAdapter @Inject constructor(private var categories: List<CategoryItem>)
    : RecyclerView.Adapter<ArbitrationCategoryAdapter.ArbitrationViewHolder>() {

    lateinit var presenter: ArbitrationLibraryPresenterImpl

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArbitrationViewHolder {
        return ArbitrationViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.category_item, parent, false))
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    override fun onBindViewHolder(holder: ArbitrationViewHolder, position: Int) {
        holder.categoryTitle.text = holder.view.context.getString(categories[position].title)
        val articlesSize = categories[position].articlesSize
        val articlesSizeInfo = String.format(holder.view.context
                .resources.getQuantityString(R.plurals.articles, articlesSize), articlesSize)
        holder.articlesSize.text = articlesSizeInfo
        holder.view.setOnClickListener { presenter.onCategoryClicked(categories[position].categoryId) }
    }

    class ArbitrationViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        val categoryTitle: TextView = view.categoryItemTitle
        val articlesSize: TextView = view.categoryItemArticleSize
    }

    fun setCategories(categories: List<CategoryItem>) {
        this.categories = categories
        notifyDataSetChanged()
    }
}