package pl.gessel.arbitrationwall2.desktop.bookmarks

import io.reactivex.disposables.Disposable
import io.realm.Realm
import io.realm.RealmResults
import io.realm.kotlin.oneOf
import pl.gessel.arbitrationwall2.realm.ApiSectionRealm
import pl.gessel.arbitrationwall2.realm.ArticleRealm

class BookmarksPresenterImpl : BookmarksPresenter {

    lateinit var bookmarksView: BookmarksPresenter.BookmarksView
    private var realm: Realm? = null
    private var articles: RealmResults<ArticleRealm>? = null
    private var disposable: Disposable? = null

    override fun init(view: BookmarksPresenter.BookmarksView) {
        this.bookmarksView = view
        realm = Realm.getDefaultInstance()
    }

    override fun getBookmarkedArticles() {
        val apiSections = realm?.where(ApiSectionRealm::class.java)?.equalTo("isBookmarked", true)?.findAll()
        val articles = apiSections?.map { apiSectionRealm -> apiSectionRealm.articleId }?.toList().orEmpty()
        val filteredArticles = realm
                ?.where(ArticleRealm::class.java)
                ?.oneOf("articleId", articles.toTypedArray())
                ?.findAll()
        bookmarksView.showArticles(filteredArticles)
    }

    override fun onBookmarkArticleClicked(articleId: Int) {
        bookmarksView.goToArticle(articleId)
    }

    override fun onPause() {
        super.onPause()
        if (realm != null && !realm!!.isClosed) {
            realm!!.close()
        }
        if (disposable != null && !disposable!!.isDisposed) {
            disposable!!.dispose()
        }
    }
}