package pl.gessel.arbitrationwall2.desktop.bookmarks

import android.support.v7.widget.RecyclerView
import android.text.Html
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import io.realm.RealmResults
import kotlinx.android.synthetic.main.search_item.view.*
import pl.gessel.arbitrationwall2.R
import pl.gessel.arbitrationwall2.realm.ArticleRealm
import javax.inject.Inject

class BookmarksAdapter @Inject constructor(private var bookmarks: RealmResults<ArticleRealm>?)
    : RecyclerView.Adapter<BookmarksAdapter.SearchViewHolder>() {

    lateinit var presenter: BookmarksPresenterImpl

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        return SearchViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.search_item, parent, false))
    }

    override fun getItemCount(): Int {
        return bookmarks?.size ?: 0
    }

    fun setArticles(searchItems: RealmResults<ArticleRealm>?) {
        this.bookmarks = searchItems
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        holder.viewTitle.text = Html.fromHtml(bookmarks?.get(position)?.title ?: "")
        holder.viewDescription.text = getArticleDescription(position)
        holder.view.setOnClickListener { presenter.onBookmarkArticleClicked(bookmarks!![position]!!.articleId) }
    }

    private fun getArticleDescription(position: Int): Spanned? {
        return Html.fromHtml(bookmarks?.get(position)?.sections?.get(0)?.text)
    }

    class SearchViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        val viewTitle: TextView = view.searchItemTitle
        val viewDescription: TextView = view.searchItemDescription
    }
}