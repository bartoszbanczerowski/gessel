package pl.gessel.arbitrationwall2.desktop.arbitrationlibrary

import io.realm.Realm
import io.realm.RealmResults
import io.realm.Sort
import pl.gessel.arbitrationwall2.injection.ArbitrationLibraryModule
import pl.gessel.arbitrationwall2.realm.ArticleRealm
import javax.inject.Inject

class ArbitrationLibraryArticlePresenterImpl @Inject constructor() : ArbitrationLibraryPresenter {

    lateinit var arbitrationView: ArbitrationLibraryPresenter.ArbitrationLibraryView
    lateinit var realm: Realm
    private var articles: RealmResults<ArticleRealm>? = null
    private var isDateSortedAscending = false
    private var isTitleSortedAscending = false

    override fun init(view: ArbitrationLibraryPresenter.ArbitrationLibraryView) {
        arbitrationView = view
        realm = Realm.getDefaultInstance()
    }

    override fun getArticlesForCategory(categoryId: Int) {
        articles = realm.where(ArticleRealm::class.java)
                .equalTo(CATEGORY_ID, categoryId)
                .findAll()

        if (categoryId == ArbitrationLibraryModule.CURRENT_NEWS_ID)
            sortArticlesByDate()
        else sortArticlesByName()

    }

    override fun sortArticlesByDate() {
        val sortedArticles = articles?.sort(DATE, if (isDateSortedAscending) Sort.DESCENDING else Sort.ASCENDING)
        arbitrationView.updateArticles(sortedArticles)
        isDateSortedAscending = !isDateSortedAscending
    }

    override fun sortArticlesByName() {
        val sortedArticles = articles?.sort(TITLE, if (isTitleSortedAscending) Sort.DESCENDING else Sort.ASCENDING)
        arbitrationView.updateArticles(sortedArticles)
        isTitleSortedAscending = !isTitleSortedAscending
    }

    override fun onArticleClicked(articleId: Int) {
        arbitrationView.goToArticle(articleId)
    }

    companion object {
        private const val TITLE = "title"
        private const val DATE = "addedDate"
        private const val CATEGORY_ID = "category.id"
    }

}