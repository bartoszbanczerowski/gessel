package pl.gessel.arbitrationwall2.desktop.bookmarks

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.AndroidSupportInjection
import io.realm.RealmResults
import kotlinx.android.synthetic.main.fragment_bookmarks.view.*
import pl.gessel.arbitrationwall2.R
import pl.gessel.arbitrationwall2.core.BaseFragment
import pl.gessel.arbitrationwall2.desktop.arbitrationlibrary.ArticleDetailFragment
import pl.gessel.arbitrationwall2.realm.ArticleRealm
import javax.inject.Inject

class BookmarksFragment : BaseFragment(), BookmarksPresenter.BookmarksView {

    @Inject
    lateinit var presenter: BookmarksPresenterImpl

    private var bookmarksAdapter = BookmarksAdapter(null)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_bookmarks, container, false)
        presenter.init(this)
        bookmarksAdapter.presenter = presenter

        presenter.getBookmarkedArticles()

        rootView.searchResults.layoutManager = LinearLayoutManager(activity)
        rootView.searchResults.adapter = bookmarksAdapter
        toolbarManager.setToolbarTitle(R.string.desktop_title_bookmarks)
        toolbarManager.showToolbarBackArrow(true)
        return rootView
    }

    override fun showArticles(articles: RealmResults<ArticleRealm>?) {
        bookmarksAdapter.setArticles(articles)
    }


    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun goToArticle(articleId: Int) {
        val fragment = ArticleDetailFragment.newInstance(articleId)
        activity!!.supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.desktopContainer, fragment, "Articles")
                .addToBackStack("Articles")
                .commit()
    }

    companion object {
        @JvmStatic
        fun newInstance() = BookmarksFragment()
    }
}

