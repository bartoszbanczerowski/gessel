package pl.gessel.arbitrationwall2.desktop.arbitrationlibrary

import android.support.annotation.StringRes
import io.realm.RealmResults
import pl.gessel.arbitrationwall2.core.BasePresenter
import pl.gessel.arbitrationwall2.realm.ArticleRealm

interface ArbitrationLibraryPresenter : BasePresenter<ArbitrationLibraryPresenter.ArbitrationLibraryView> {

    fun getArticlesForCategory(categoryId: Int) {}
    fun sortArticlesByDate() {}
    fun sortArticlesByName() {}
    fun onCategoryClicked(categoryId: Int) {}
    fun onArticleClicked(articleId: Int) {}

    interface ArbitrationLibraryView : BasePresenter.View {
        fun updateCategories(categories: List<CategoryItem>) {}
        fun updateArticles(articles: RealmResults<ArticleRealm>?) {}
        fun goToCategoryArticles(categoryId: Int, @StringRes categoryName: Int) {}
        fun goToArticle(articleId: Int) {}
    }
}