package pl.gessel.arbitrationwall2.desktop

import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.app_bar_desktop.*
import pl.gessel.arbitrationwall2.R
import pl.gessel.arbitrationwall2.core.ToolbarManager
import pl.gessel.arbitrationwall2.desktop.search.SearchFragment
import javax.inject.Inject


class DesktopActivity : AppCompatActivity(), HasSupportFragmentInjector,
        DesktopPresenter.DesktopView, ToolbarManager {

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_desktop)
        setSupportActionBar(toolbar)

        supportFragmentManager.beginTransaction()
                .replace(R.id.desktopContainer, DesktopFragment(), "Desktop")
                .commit()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0)
            supportFragmentManager.popBackStackImmediate()
        else super.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.desktop, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.action_search -> {
                goToSearch()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentInjector

    override fun goToSearch() {
        val fragment = SearchFragment()
        supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.desktopContainer, fragment, "Search")
                .addToBackStack("Search")
                .commit()
    }

    override fun setToolbarTitle(@StringRes title: Int) {
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.title = getString(title)
    }

    override fun setToolbarTitle(title: String?) {
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.title = title
    }

    override fun showToolbarBackArrow(shouldShow: Boolean) {
        if (shouldShow) {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowHomeEnabled(true)
        } else {
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
            supportActionBar?.setDisplayShowHomeEnabled(false)
        }
    }

    override fun showMenuOption(shouldShow: Boolean) {
//        supportActionBar?.displayOptions = shouldShow
    }
}
