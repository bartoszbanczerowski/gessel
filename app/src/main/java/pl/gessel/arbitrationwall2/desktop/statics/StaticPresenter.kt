package pl.gessel.arbitrationwall2.desktop.statics

import pl.gessel.arbitrationwall2.core.BasePresenter
import pl.gessel.arbitrationwall2.realm.ArticleRealm

interface StaticPresenter : BasePresenter<StaticPresenter.StaticView> {

    fun getArticle(articleId: Int): ArticleRealm?

    interface StaticView: BasePresenter.View {
    }
}