package pl.gessel.arbitrationwall2.desktop

import pl.gessel.arbitrationwall2.core.BasePresenter


interface DesktopPresenter : BasePresenter<DesktopPresenter.DesktopView> {
    fun onDesktopItemClicked(position: Int)

    interface DesktopView : BasePresenter.View{
        fun goToAboutGessel(){}
        fun goToUsersManual(){}
        fun goToSettings(){}
        fun goToArbitrationWall(){}
        fun goToAboutTheWall(){}
        fun goToBookmarks(){}
        fun goToSearch(){}
    }
}