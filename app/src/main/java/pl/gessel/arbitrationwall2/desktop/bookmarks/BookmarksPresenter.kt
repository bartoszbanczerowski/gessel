package pl.gessel.arbitrationwall2.desktop.bookmarks

import io.realm.RealmResults
import pl.gessel.arbitrationwall2.core.BasePresenter
import pl.gessel.arbitrationwall2.realm.ArticleRealm

interface BookmarksPresenter : BasePresenter<BookmarksPresenter.BookmarksView> {

    fun getBookmarkedArticles() {}
    fun onBookmarkArticleClicked(articleId: Int) {}

    interface BookmarksView : BasePresenter.View {
        fun showArticles(articles: RealmResults<ArticleRealm>?) {}
        fun goToArticle(articleId: Int) {}
    }
}