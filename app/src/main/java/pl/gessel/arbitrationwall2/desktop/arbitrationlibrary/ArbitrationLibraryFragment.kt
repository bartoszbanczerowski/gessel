package pl.gessel.arbitrationwall2.desktop.arbitrationlibrary

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_arbitration_library.view.*
import pl.gessel.arbitrationwall2.R
import pl.gessel.arbitrationwall2.core.BaseFragment
import javax.inject.Inject

class ArbitrationLibraryFragment : BaseFragment(), ArbitrationLibraryPresenter.ArbitrationLibraryView {

    @Inject
    lateinit var presenter: ArbitrationLibraryPresenterImpl

    @Inject
    lateinit var arbitrationCategoryAdapter: ArbitrationCategoryAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_arbitration_library, container, false)
        arbitrationCategoryAdapter.presenter = presenter
        rootView.categories.adapter = arbitrationCategoryAdapter
        rootView.categories.layoutManager = LinearLayoutManager(rootView.context)
        presenter.init(this)
        toolbarManager.setToolbarTitle(R.string.desktop_title_arbitration_library)
        toolbarManager.showToolbarBackArrow(true)
        return rootView
    }


    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun updateCategories(categories: List<CategoryItem>) {
        arbitrationCategoryAdapter.setCategories(categories)
    }

    override fun goToCategoryArticles(categoryId: Int, categoryName: Int) {
        var fragment = ArbitrationLibraryArticleFragment.newInstance(categoryId, categoryName)
        activity!!.supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.desktopContainer, fragment, "Articles")
                .addToBackStack("Articles")
                .commit()
    }
}
