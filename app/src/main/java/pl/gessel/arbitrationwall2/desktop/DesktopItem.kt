package pl.gessel.arbitrationwall2.desktop

import android.support.annotation.DrawableRes
import android.support.annotation.StringRes

class DesktopItem constructor(@StringRes var title: Int,
                              @DrawableRes var icon: Int,
                              var desktopItemType: DesktopItemType)