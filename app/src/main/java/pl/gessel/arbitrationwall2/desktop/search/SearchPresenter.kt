package pl.gessel.arbitrationwall2.desktop.search

import pl.gessel.arbitrationwall2.core.BasePresenter
import pl.gessel.arbitrationwall2.realm.ArticleRealm

interface SearchPresenter : BasePresenter<SearchPresenter.SearchView> {

    fun searchArticles(query: String) {}
    fun onSearchArticleClicked(articleId: Int) {}

    interface SearchView : BasePresenter.View {
        fun showArticles(articles: List<ArticleRealm>?) {}
        fun goToArticle(articleId: Int) {}
    }
}