package pl.gessel.arbitrationwall2.desktop.search

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_search.view.*
import pl.gessel.arbitrationwall2.R
import pl.gessel.arbitrationwall2.core.BaseFragment
import pl.gessel.arbitrationwall2.desktop.arbitrationlibrary.ArticleDetailFragment
import pl.gessel.arbitrationwall2.realm.ArticleRealm
import javax.inject.Inject


class SearchFragment : BaseFragment(), SearchPresenter.SearchView {

    @Inject
    lateinit var presenter: SearchPresenterImpl

    private var searchAdapter = SearchAdapter(null)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_search, container, false)
        presenter.init(this)
        searchAdapter.searchPresenter = presenter

        rootView.searchQuery.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(query: Editable?) {
                presenter.searchArticles(query = query.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })
        rootView.searchResults.layoutManager = LinearLayoutManager(activity)
        rootView.searchResults.adapter = searchAdapter
        toolbarManager.setToolbarTitle(R.string.desktop_title_search)
        toolbarManager.showToolbarBackArrow(true)
        return rootView
    }

    override fun showArticles(articles: List<ArticleRealm>?) {
        searchAdapter.setArticles(articles)
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun goToArticle(articleId: Int) {
        val fragment = ArticleDetailFragment.newInstance(articleId)
        activity!!.supportFragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .replace(R.id.desktopContainer, fragment, "Articles")
                .addToBackStack("Articles")
                .commit()
    }
}
