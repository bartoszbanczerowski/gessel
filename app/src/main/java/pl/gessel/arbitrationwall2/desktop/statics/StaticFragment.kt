package pl.gessel.arbitrationwall2.desktop.statics

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_about_gessel.view.*
import pl.gessel.arbitrationwall2.R
import pl.gessel.arbitrationwall2.core.BaseFragment
import pl.gessel.arbitrationwall2.core.SectionCreator
import timber.log.Timber
import javax.inject.Inject


class StaticFragment : BaseFragment(), StaticPresenter.StaticView {

    @Inject
    lateinit var presenter: StaticPresenterImpl

    val articleId: Int = 0

    private val sectionCreator: SectionCreator = SectionCreator()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_about_gessel, container, false)
        val articleId = arguments?.getInt(STATIC_FRAGMENT_ID) ?: 0
        setTitle(articleId)
        presenter.init(this)
        val article = presenter.getArticle(articleId)

        val mapApiSections = presenter.mapApiSections(article!!.sections)
        mapApiSections.forEachIndexed { position, apiSectionRealm ->
            val createSection = sectionCreator.createSection(rootView.context, apiSectionRealm)
            rootView.sectionLinearLayout.addView(createSection)
        }

        Timber.d("${rootView.sectionLinearLayout.childCount}")
        setToolbarTitle(articleId)
        return rootView
    }

    private fun setToolbarTitle(articleId: Int) {
        val title: Int = when (articleId) {
            ABOUT_THE_WALL_ID -> R.string.desktop_title_about_the_wall
            ABOUT_GESSEL_ID -> R.string.desktop_title_about_gessel
            USERS_MANUAL_ID -> R.string.desktop_title_users_manual
            else -> throw Exception("Unknown Article Id")
        }
        toolbarManager.setToolbarTitle(title)
        toolbarManager.showToolbarBackArrow(true)
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onResume() {
        super.onResume()
    }


    private fun setTitle(articleId: Int) {
        val title = when (articleId) {
            ABOUT_THE_WALL_ID -> getString(R.string.desktop_title_about_the_wall)
            ABOUT_GESSEL_ID -> getString(R.string.desktop_title_about_gessel)
            USERS_MANUAL_ID -> getString(R.string.desktop_title_users_manual)
            else -> throw Error("ArticleId for Title Unknown")
        }
    }

    companion object {
        const val STATIC_FRAGMENT_ID = "STATIC_FRAGMENT_ID"
        const val ABOUT_THE_WALL_ID = 339
        const val ABOUT_GESSEL_ID = 338
        const val USERS_MANUAL_ID = 347
    }
}
