package pl.gessel.arbitrationwall2.desktop

enum class DesktopItemType(position: Int) {
    ARBITRATION_LIBRARY(0),
    ABOUT_THE_WALL(1),
    ABOUT_GESSEL(2),
    USERS_MANUAL(3),
    BOOKMARKS(4),
    SETTINGS(5)
}