package pl.gessel.arbitrationwall2.desktop.statics

import io.realm.Realm
import io.realm.RealmList
import pl.gessel.arbitrationwall2.networking.models.ApiSectionsModel
import pl.gessel.arbitrationwall2.realm.ApiSectionRealm
import pl.gessel.arbitrationwall2.realm.ArticleRealm
import javax.inject.Inject

class StaticPresenterImpl @Inject constructor() : StaticPresenter {

    private lateinit var view: StaticPresenter.StaticView


    override fun init(view: StaticPresenter.StaticView) {
        this.view = view
    }

    override fun getArticle(articleId: Int): ArticleRealm? {
        return Realm.getDefaultInstance().where(ArticleRealm::class.java)
                .equalTo("category.id", articleId.toInt())
                .findFirst()
    }

    fun mapApiSections(apiSections: RealmList<ApiSectionRealm>): List<ApiSectionsModel> {
        val sections = ArrayList<ApiSectionsModel>()
        apiSections.forEach { apiSection: ApiSectionRealm ->
            sections.add(
                ApiSectionsModel(
                    text = apiSection.text,
                    type = apiSection.type,
                    link = apiSection.link,
                    articleId = apiSection.articleId,
                    isBookmarked = apiSection.isBookmarked)
            )
        }
        return sections
    }
}
