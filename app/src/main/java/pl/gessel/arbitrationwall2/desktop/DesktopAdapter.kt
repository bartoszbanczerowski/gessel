package pl.gessel.arbitrationwall2.desktop

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import kotlinx.android.synthetic.main.desktop_item.view.*
import pl.gessel.arbitrationwall2.R
import javax.inject.Inject

class DesktopAdapter @Inject constructor(private val desktopItems: List<DesktopItem>)
    : RecyclerView.Adapter<DesktopAdapter.DesktopViewHolder>() {

    lateinit var desktopPresenter: DesktopPresenterImpl

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DesktopViewHolder {
        return DesktopViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.desktop_item, parent, false))
    }

    override fun getItemCount(): Int {
        return desktopItems.size
    }

    override fun onBindViewHolder(holder: DesktopViewHolder, position: Int) {
        holder.viewIcon.setImageDrawable(ContextCompat.getDrawable(holder.view.context, desktopItems[position].icon))
        holder.viewIcon.setOnClickListener { desktopPresenter.onDesktopItemClicked(position) }
    }

    class DesktopViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        val viewIcon: ImageView = view.desktopItemIcon
    }
}