package pl.gessel.arbitrationwall2.desktop.search

import com.squareup.picasso.Picasso
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import io.realm.RealmResults
import io.realm.kotlin.oneOf
import pl.gessel.arbitrationwall2.realm.ApiSectionRealm
import pl.gessel.arbitrationwall2.realm.ArticleRealm
import java.util.concurrent.TimeUnit

class SearchPresenterImpl : SearchPresenter {

    lateinit var searchView: SearchPresenter.SearchView
    private var realm: Realm? = null
    private var articles: RealmResults<ArticleRealm>? = null
    private var disposable: Disposable? = null
    private var picasso: Picasso = Picasso.get()

    override fun init(view: SearchPresenter.SearchView) {
        this.searchView = view
        realm = Realm.getDefaultInstance()
    }

    override fun searchArticles(query: String) {
        disposable = Observable.just(query)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .debounce(300, TimeUnit.MILLISECONDS)
                .filter { q: String ->
                    !q.isEmpty()
                }
                .subscribe { filterdQuery: String? ->
                    if (filterdQuery.isNullOrEmpty()) filterdQuery.plus("")
                    articles = realm!!.where(ArticleRealm::class.java)
                            .contains("title", filterdQuery)
                            .findAll()
                    val apiSections = realm!!.where(ApiSectionRealm::class.java)
                            .contains("text", filterdQuery)
                            .findAll()

                    val filteredArticlesIds = apiSections
                            .map { apiSectionRealm -> apiSectionRealm.articleId }
                            .toList()

                    val filteredArticles = realm!!.where(ArticleRealm::class.java)
                            .oneOf("articleId", filteredArticlesIds.toTypedArray())
                            .findAll()
                    val toHashSet = articles?.toHashSet()
                    toHashSet?.addAll(filteredArticles.toHashSet())

                    searchView.showArticles(toHashSet?.toList())
                }
    }

    override fun onSearchArticleClicked(articleId: Int) {
        searchView.goToArticle(articleId)
    }

    override fun onPause() {
        super.onPause()
        if (realm != null && !realm!!.isClosed) {
            realm!!.close()
        }
        if (disposable != null && !disposable!!.isDisposed) {
            disposable!!.dispose()
        }
    }
}