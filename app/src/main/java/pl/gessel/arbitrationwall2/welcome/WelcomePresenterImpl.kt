package pl.gessel.arbitrationwall2.welcome

import android.content.SharedPreferences
import android.net.ConnectivityManager
import io.reactivex.disposables.Disposable
import io.realm.Realm
import io.realm.RealmList
import pl.gessel.arbitrationwall2.core.SchedulerProvider
import pl.gessel.arbitrationwall2.networking.ArticleService
import pl.gessel.arbitrationwall2.networking.models.ApiSections
import pl.gessel.arbitrationwall2.networking.models.Article
import pl.gessel.arbitrationwall2.realm.*
import timber.log.Timber


open class WelcomePresenterImpl constructor(
        private val sharedPreferences: SharedPreferences,
        private val connectivityManager: ConnectivityManager,
        private val schedulerProvider: SchedulerProvider,
        private val articleService: ArticleService) : WelcomePresenter {

    private var disposable: Disposable? = null
    private var disposableArticle: Disposable? = null
    private lateinit var welcomeView: WelcomePresenter.WelcomeView
    private lateinit var realm: Realm
    private val isFirstAppLaunch = sharedPreferences.getBoolean(FIRST_APP_LAUNCH, true)
    private var articlesToDownload = mutableListOf<Int>()
    private var articleToDownload: Int = 0
    private var downloadProgress: Int = 0

    override fun init(view: WelcomePresenter.WelcomeView) {
        this.welcomeView = view
        initializePresenter()
    }

    private fun initializePresenter() {
        if (isFirstAppLaunch && !isOnline()) {
            this.welcomeView.showContentError()
            return
        }
        realm = Realm.getDefaultInstance()
        getArticles()
    }

    private fun isOnline(): Boolean {
        val netInfo = connectivityManager.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }

    override fun onPause() {
        welcomeView.hideContentError()
    }

    override fun onDestroy() {
        if (disposable != null && !disposable?.isDisposed!!) {
            disposable?.dispose()
        }
        if (disposableArticle != null && !disposableArticle?.isDisposed!!) {
            disposableArticle?.dispose()
        }
    }

    private fun getArticles() {
        disposable = articleService.getArticles(ArticleService.GESSEL_APPLICATION,
                ArticleService.POST_IDS_ACTION)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(
                        { response ->
                            val articles = response.body()!!.articles
                            saveRealmArticles(articles.map { articleListItem ->
                                ArticleListItemRealm(articleId = articleListItem.articleId,
                                        createDate = articleListItem.createDate,
                                        modifiedDate = articleListItem.modifiedDate)
                            }.toList())
                            Timber.d("SIEMA: ${articles.size}")
                            findArticleRealm()
                        },
                        { error ->
                            Timber.d("Article not downloaded: " + error.message)
                        })
    }


    private fun saveRealmArticles(articleList: List<ArticleListItemRealm>) {
        deleteAllArticles(articleList)

        realm.executeTransaction { realm -> realm.copyToRealmOrUpdate(articleList) }
    }

    private fun deleteAllArticles(articleList: List<ArticleListItemRealm>) {
        val newArticles = articleList.map { articleListItemRealm -> articleListItemRealm.articleId }.toList()

        val articlesListItemToDelete = realm.where(ArticleListItemRealm::class.java).not().`in`("articleId", newArticles.toTypedArray()).findAll()
        articlesListItemToDelete.deleteAllFromRealm()

        val articlesToDelete = realm.where(ArticleRealm::class.java).not().`in`("articleId", newArticles.toTypedArray()).findAll()
        articlesToDelete.deleteAllFromRealm()
    }

    private fun findArticleRealm() {
        val findAll = realm.where(ArticleListItemRealm::class.java).findAll()

        findAll.forEachIndexed { _, articleListItem ->
            val articleRealm = realm.where(ArticleRealm::class.java).equalTo("articleId", articleListItem.articleId).findFirst()
            if (!isArticleValid(articleListItem, articleRealm)) {
                articlesToDownload.add(articleListItem.articleId)
            }
        }

        if (articlesToDownload.size > 0) {
            getArticle()
        } else {
            realm.close()
            welcomeView.articlesDownloaded()
        }
    }

    private fun isArticleValid(articleListItem: ArticleListItemRealm, articleRealm: ArticleRealm?): Boolean =
            articleRealm != null && !articleRealm.modifiedDate.before(articleListItem.modifiedDate)

    private fun getArticle() {
        downloadProgress = (articleToDownload * 100) / articlesToDownload.size
        val articleId = articlesToDownload[articleToDownload]
        articleToDownload++

        disposableArticle = articleService.getArticle(ArticleService.GESSEL_APPLICATION, ArticleService.GET_POST_ACTION, articleId)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(
                        { response ->
                            welcomeView.showProgressDownload(downloadProgress)
                            val article = response.body()!!.article
                            article.articleId = articleId
                            saveRealmArticle(article)
                            if (articlesToDownload.size > articleToDownload) {
                                getArticle()
                            } else {
                                sharedPreferences.edit().putBoolean(FIRST_APP_LAUNCH, false).apply()
                                welcomeView.articlesDownloaded()
                            }
                        },
                        { error ->
                            Timber.d("Article $articleId not downloaded properly: " + error.message)
                        })
    }

    private fun saveRealmArticle(article: Article) {
        realm.executeTransaction { realm ->
            realm.copyToRealmOrUpdate(ArticleRealm(
                    articleId = article.articleId,
                    category = CategoryRealm(article.category.id, article.category.name),
                    modifiedDate = article.modifiedDate,
                    addedDate = article.addedDate,
                    pdfUrl = article.pdfUrl,
                    title = article.title,
                    sections = mapApiSections(article.sections, article.articleId)))
        }
    }

    private fun mapApiSections(apiSections: List<ApiSections>, articleId: Int): RealmList<ApiSectionRealm> {
        val realmList = RealmList<ApiSectionRealm>()
        apiSections.forEach { apiSection: ApiSections? ->
            realmList.add(ApiSectionRealm(
                    text = apiSection?.text ?: "",
                    type = apiSection?.type ?: ApiSectionTypeRealm.UNKNOWN.toString(),
                    link = apiSection?.link ?: "",
                    articleId = articleId,
                    isBookmarked = false))
        }
        return realmList
    }


    companion object {
        private const val FIRST_APP_LAUNCH = "firstAppLaunch"
    }
}
