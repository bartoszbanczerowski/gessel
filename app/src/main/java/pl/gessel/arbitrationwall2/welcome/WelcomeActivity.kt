package pl.gessel.arbitrationwall2.welcome

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_welcome.*
import pl.gessel.arbitrationwall2.R
import pl.gessel.arbitrationwall2.desktop.DesktopActivity
import javax.inject.Inject

class WelcomeActivity : AppCompatActivity(), WelcomePresenter.WelcomeView {

    @Inject
    lateinit var presenter: WelcomePresenterImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        presenter.init(this)
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onPause() {
        super.onPause()
        presenter.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun showProgressDownload(progressDownload: Int) {
        synchronizationResult.text = "$progressDownload %"
    }

    override fun showContentError() {
        super.showContentError()
    }

    override fun hideContentError() {
        super.hideContentError()
    }

    override fun articlesDownloaded() {
        val intent = Intent(this, DesktopActivity::class.java)
        startActivity(intent)
        finish()
    }

}
