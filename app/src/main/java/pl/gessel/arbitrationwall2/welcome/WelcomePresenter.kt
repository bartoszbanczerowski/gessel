package pl.gessel.arbitrationwall2.welcome

import pl.gessel.arbitrationwall2.core.BasePresenter

interface WelcomePresenter : BasePresenter<WelcomePresenter.WelcomeView> {

    override fun init(view: WelcomeView)

    interface WelcomeView : BasePresenter.View {
        fun showProgressDownload(progressDownload: Int)
        fun articlesDownloaded() {}
    }

}