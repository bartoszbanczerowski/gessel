package pl.gessel.arbitrationwall2.core

import android.content.Context
import android.support.v4.app.Fragment
import pl.gessel.arbitrationwall2.desktop.DesktopActivity


open class BaseFragment : Fragment() {

    lateinit var toolbarManager: ToolbarManager

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            toolbarManager = activity as DesktopActivity
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + " must implement ToolbarManager")
        }
    }
}
