package pl.gessel.arbitrationwall2.core

import io.reactivex.Scheduler

interface BaseSchedulerProvider {
   fun computation(): Scheduler
   fun io(): Scheduler
   fun ui(): Scheduler
}