package pl.gessel.arbitrationwall2.core

import android.support.annotation.StringRes

interface ToolbarManager {

    fun setToolbarTitle(@StringRes title: Int) {}
    fun setToolbarTitle(title: String?) {}
    fun showToolbarBackArrow(shouldShow: Boolean) {}
    fun showMenuOption(shouldShow: Boolean){}
}