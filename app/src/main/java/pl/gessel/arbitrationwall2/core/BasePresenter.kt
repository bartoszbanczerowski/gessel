package pl.gessel.arbitrationwall2.core

interface BasePresenter<in View> {

    fun init(view : View)
    fun onResume() {}
    fun onPause() {}
    fun onDestroy() {}

    interface View {
        fun clearResults() {}
        fun showContentError() {}
        fun hideContentError() {}
    }
}