package pl.gessel.arbitrationwall2.core

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.annotation.DrawableRes
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.text.Html
import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.bookmark_view.view.*
import kotlinx.android.synthetic.main.link_view.view.*
import pl.gessel.arbitrationwall2.R
import pl.gessel.arbitrationwall2.networking.models.ApiSectionsModel
import pl.gessel.arbitrationwall2.realm.ApiSectionTypeRealm
import javax.inject.Inject
import android.util.DisplayMetrics

class SectionCreator @Inject constructor() {

    fun createSection(context: Context, apiSectionsRealm: ApiSectionsModel): View {
        when (apiSectionsRealm.type) {
            ApiSectionTypeRealm.TEXT.type -> {
                return createTextSection(context, apiSectionsRealm)
            }
            ApiSectionTypeRealm.HEADER.type -> {
                return createHeaderSection(context, apiSectionsRealm)
            }
            ApiSectionTypeRealm.SUB_HEADER.type -> {
                return createSubHeaderSection(context, apiSectionsRealm)
            }
            ApiSectionTypeRealm.SUB_SUB_HEADER.type -> {
                return createSubSubHeaderSection(context, apiSectionsRealm)
            }
            ApiSectionTypeRealm.SUB_SUB_SUB_HEADER.type -> {
                return createSubSubSubHeaderSection(context, apiSectionsRealm)
            }
            ApiSectionTypeRealm.SUB_SUB_SUB_SUB_HEADER.type -> {
                return createSubSubSubHeaderSection(context, apiSectionsRealm)
            }
            ApiSectionTypeRealm.LINK.type -> {
                return createLinkSection(context, apiSectionsRealm)
            }
            ApiSectionTypeRealm.IMAGE.type -> {
                return createImageSection(context, apiSectionsRealm)
            }
            ApiSectionTypeRealm.FULL_IMAGE.type -> {
                return createImageSection(context, apiSectionsRealm)
            }

            ApiSectionTypeRealm.LIST_UL.type -> {
                return createUlListSection(context, apiSectionsRealm)
            }
            ApiSectionTypeRealm.LIST_OL.type -> {
                return createOlListSection(context, apiSectionsRealm)
            }
            ApiSectionTypeRealm.MAIL.type -> {
                return createMailSection(context, apiSectionsRealm)
            }
            ApiSectionTypeRealm.TABLE.type -> {
                return createTableSection(context, apiSectionsRealm)
            }
            ApiSectionTypeRealm.TEXT_BOLD.type -> {
                return createBoldTextSection(context, apiSectionsRealm)
            }
            else -> return createTextSection(context, apiSectionsRealm)
//                throw Exception("unknown Section Type! Section: ${apiSectionsRealm.type}")
        }
    }

    private fun createTextSection(
        context: Context,
        apiSectionRealm: ApiSectionsModel
    ): View {
        val bookmarkView = BookmarkView(context)

        val string = apiSectionRealm.text
            .replace("\r\n", "")

        bookmarkView.sectionText.text = Html.fromHtml(string)
        bookmarkView.sectionText.textSize = 16f
        val bottomPadding = convertDpToPixel(8f, context).toInt()
        bookmarkView.sectionText.setPadding(8, 0, 8, bottomPadding)
        bookmarkView.sectionText.setTextColor(ContextCompat.getColor(context, R.color.text))
        bookmarkView.sectionText.layoutParams = layoutParams

        if (string.toLowerCase().startsWith("tel:")) {
            Linkify.addLinks(bookmarkView.sectionText, Linkify.PHONE_NUMBERS)
        }

        val typeface = ResourcesCompat.getFont(context, R.font.opensans_light)
        bookmarkView.sectionText.typeface = typeface

        if (apiSectionRealm.isBookmarked) bookmarkView.bookmarkImage.visibility = View.VISIBLE

        return bookmarkView
    }

    private fun createBoldTextSection(
        context: Context,
        apiSectionRealm: ApiSectionsModel
    ): View {
        val bookmarkView = BookmarkView(context)
        val string = apiSectionRealm.text
            .replace("\r\n", "")

        bookmarkView.sectionText.text = Html.fromHtml(string)
        bookmarkView.sectionText.textSize = 16f
        val bottomPadding = convertDpToPixel(8f, context).toInt()
        bookmarkView.sectionText.setPadding(8, 0, 8, bottomPadding)
        bookmarkView.sectionText.setTextColor(ContextCompat.getColor(context, R.color.text))
        bookmarkView.sectionText.setLinkTextColor(
            ContextCompat.getColor(
                context,
                R.color.link_color
            )
        )
        bookmarkView.sectionText.layoutParams = layoutParams

        if (string.toLowerCase().contains("Contact: Tel")) {
            Linkify.addLinks(bookmarkView.sectionText, Linkify.PHONE_NUMBERS)
        }

        val typeface = ResourcesCompat.getFont(context, R.font.opensans_regular)
        bookmarkView.sectionText.typeface = typeface

        if (apiSectionRealm.isBookmarked) bookmarkView.bookmarkImage.visibility = View.VISIBLE

        return bookmarkView
    }

    private fun createHeaderSection(
        context: Context,
        apiSectionRealm: ApiSectionsModel
    ): View {
        val bookmarkView = BookmarkView(context)

        val bottomPadding = convertDpToPixel(8f, context).toInt()
        bookmarkView.sectionText.text = Html.fromHtml(apiSectionRealm.text)
        bookmarkView.sectionText.textSize = 20f
        bookmarkView.sectionText.gravity = Gravity.CENTER
        bookmarkView.sectionText.setPadding(8, 0, 8, bottomPadding)
        bookmarkView.sectionText.setTextColor(ContextCompat.getColor(context, R.color.text))
        bookmarkView.sectionText.layoutParams = layoutParams

        val typeface = ResourcesCompat.getFont(context, R.font.opensans_regular)
        bookmarkView.sectionText.typeface = typeface

        if (apiSectionRealm.isBookmarked) bookmarkView.bookmarkImage.visibility = View.VISIBLE

        return bookmarkView
    }

    private fun createSubHeaderSection(
        context: Context,
        apiSectionRealm: ApiSectionsModel
    ): View {
        val bookmarkView = BookmarkView(context)
        val bottomPadding = convertDpToPixel(8f, context).toInt()
        bookmarkView.sectionText.text = Html.fromHtml(apiSectionRealm.text)
        bookmarkView.sectionText.textSize = 19f
        bookmarkView.sectionText.gravity = Gravity.CENTER
        bookmarkView.sectionText.setPadding(8, 0, 8, bottomPadding)
        bookmarkView.sectionText.setTextColor(ContextCompat.getColor(context, R.color.text))
        bookmarkView.sectionText.layoutParams = layoutParams

        val typeface = ResourcesCompat.getFont(context, R.font.opensans_regular)
        bookmarkView.sectionText.typeface = typeface

        if (apiSectionRealm.isBookmarked) bookmarkView.bookmarkImage.visibility = View.VISIBLE

        return bookmarkView
    }

    private fun createSubSubHeaderSection(
        context: Context,
        apiSectionRealm: ApiSectionsModel
    ): View {
        val bookmarkView = BookmarkView(context)
        val bottomPadding = convertDpToPixel(8f, context).toInt()
        bookmarkView.sectionText.text = Html.fromHtml(apiSectionRealm.text)
        bookmarkView.sectionText.textSize = 18f
        bookmarkView.sectionText.setPadding(8, 0, 8, bottomPadding)
        bookmarkView.sectionText.setTextColor(ContextCompat.getColor(context, R.color.text))
        bookmarkView.sectionText.layoutParams = layoutParams

        if (apiSectionRealm.isBookmarked) bookmarkView.bookmarkImage.visibility = View.VISIBLE

        return bookmarkView
    }

    private fun createSubSubSubHeaderSection(
        context: Context,
        apiSectionRealm: ApiSectionsModel
    ): View {
        val bookmarkView = BookmarkView(context)
        bookmarkView.sectionText.text = Html.fromHtml(apiSectionRealm.text)
        bookmarkView.sectionText.textSize = 17f
        val bottomPadding = convertDpToPixel(8f, context).toInt()
        bookmarkView.sectionText.setPadding(8, 0, 8, bottomPadding)
        bookmarkView.sectionText.setTextColor(ContextCompat.getColor(context, R.color.text))
        bookmarkView.sectionText.layoutParams = layoutParams

        if (apiSectionRealm.isBookmarked) bookmarkView.bookmarkImage.visibility = View.VISIBLE

        return bookmarkView
    }

    private fun createImageSection(
        context: Context,
        apiSectionRealm: ApiSectionsModel
    ): View {
        val imageView = ImageView(context)

        imageLayoutParams.gravity = Gravity.CENTER

        imageView.layoutParams = imageLayoutParams

        imageView.setPadding(8, 0, 8, 0)

        Picasso.get().load(apiSectionRealm.link)
            .resize(480, 480)
            .into(imageView)

        return imageView
    }

    private fun createMailSection(
        context: Context,
        apiSectionRealm: ApiSectionsModel
    ): View {
        val fromHtml = Html.fromHtml("<a href=\"" + apiSectionRealm.link + "\">" + apiSectionRealm.text + "</a>")

        val linkView = LinkView(context)
        linkView.linkImage.setImageResource(R.drawable.email_icon)
        linkView.linkText.text = fromHtml
        linkView.linkText.movementMethod = LinkMovementMethod.getInstance()
        Linkify.addLinks(linkView.linkText, Linkify.EMAIL_ADDRESSES)
        return linkView
    }

    private fun createLinkSection(
        context: Context,
        apiSectionRealm: ApiSectionsModel
    ): View {
        val fromHtml = Html.fromHtml("<a href=\"" + apiSectionRealm.link + "\">" + apiSectionRealm.text + "</a>")

        val linkView = LinkView(context)
        linkView.linkImage.setImageResource(R.drawable.link_icon)
        linkView.linkText.text = fromHtml
        linkView.linkText.movementMethod = LinkMovementMethod.getInstance()
        return linkView
    }

    private fun createUlListSection(
        context: Context,
        apiSectionRealm: ApiSectionsModel
    ): View {
        val bookmarkView = BookmarkView(context)

        val string =
            "<p align=justify style=\"padding-top: 0cm;padding-bottom: 0cm;\">"
                .plus(apiSectionRealm.text)
                .plus("</p>")
                .replace("\r\n", "")

        val convertDpToPixel = convertDpToPixel(20f, context)

        bookmarkView.sectionText.text = Html.fromHtml(string)
        bookmarkView.sectionText.textSize = 16f
        bookmarkView.sectionText.textAlignment = View.TEXT_ALIGNMENT_GRAVITY
        bookmarkView.sectionText.setPadding(convertDpToPixel.toInt(), 0, 8, 0)
        bookmarkView.sectionText.setTextColor(ContextCompat.getColor(context, R.color.table_text))
        bookmarkView.sectionText.layoutParams = layoutParams
        val typeface = ResourcesCompat.getFont(context, R.font.opensans_light)
        bookmarkView.sectionText.typeface = typeface

        if (apiSectionRealm.isBookmarked) bookmarkView.bookmarkImage.visibility = View.VISIBLE

        return bookmarkView
    }

    private fun createOlListSection(
        context: Context,
        apiSectionRealm: ApiSectionsModel
    ): View {
        val bookmarkView = BookmarkView(context)

        val string =
            "<p align=justify style=\"padding-top: 0cm;padding-bottom: 0cm;\">"
                .plus(apiSectionRealm.text)
                .plus("</p>")
                .replace("\r\n", "")

        val convertDpToPixel = convertDpToPixel(30f, context)

        bookmarkView.sectionText.text = Html.fromHtml(string)
        bookmarkView.sectionText.textSize = 16f
        bookmarkView.sectionText.textAlignment = View.TEXT_ALIGNMENT_GRAVITY
        bookmarkView.sectionText.setPadding(convertDpToPixel.toInt(), 0, 8, 0)
        bookmarkView.sectionText.setTextColor(ContextCompat.getColor(context, R.color.table_text))
        bookmarkView.sectionText.layoutParams = layoutParams
        val typeface = ResourcesCompat.getFont(context, R.font.opensans_light)
        bookmarkView.sectionText.typeface = typeface

        if (apiSectionRealm.isBookmarked) bookmarkView.bookmarkImage.visibility = View.VISIBLE

        return bookmarkView
    }

    private fun createTableSection(
        context: Context,
        apiSectionRealm: ApiSectionsModel
    ): View {
        val bookmarkView = BookmarkView(context)
        bookmarkView.sectionText.visibility = View.GONE
        bookmarkView.sectionTable.setPadding(8, 0, 8, 0)
        bookmarkView.sectionTable.loadData(apiSectionRealm.text, MIME_TYPE, ENCODING)
        if (apiSectionRealm.isBookmarked) bookmarkView.bookmarkImage.visibility = View.VISIBLE

        return bookmarkView
    }

    private fun setDrawableDimensions(context: Context, @DrawableRes drawableIcon: Int): Drawable? {
        val dimensionInDp = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            24f,
            context.resources.displayMetrics
        ).toInt()
        val drawable = ContextCompat.getDrawable(context, drawableIcon)
        drawable?.setBounds(0, 0, dimensionInDp, dimensionInDp)
        return drawable
    }

    companion object {
        private val layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
        )
        private val imageLayoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
        )
        private const val MIME_TYPE = "text/html"
        private const val ENCODING = "utf-8"
    }

   private fun convertDpToPixel(dp: Float, context: Context): Float = dp * (context.resources.displayMetrics.densityDpi
            .toFloat() / DisplayMetrics.DENSITY_DEFAULT)

}
