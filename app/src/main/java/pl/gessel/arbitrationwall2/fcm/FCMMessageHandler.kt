package pl.gessel.arbitrationwall2.fcm

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import pl.gessel.arbitrationwall2.R
import pl.gessel.arbitrationwall2.welcome.WelcomeActivity

class FCMMessageHandler : FirebaseMessagingService() {

    private var handler: Handler? = null

    override fun onCreate() {
        super.onCreate()
        handler = Handler()
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        prepareNotification(remoteMessage?.notification?.title, remoteMessage?.notification?.body)
    }


    private fun prepareNotification(title: String?, body: String?) {
        handler!!.post {
            val builder: NotificationCompat.Builder?
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                builder = NotificationCompat.Builder(this, createNotificationChannel())
            } else {
                builder = NotificationCompat.Builder(this@FCMMessageHandler)
            }
            builder.setContentTitle(title)
                    .setSmallIcon(R.drawable.icon_app)
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                    .setChannelId(GESSEL_CHANNEL)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setAutoCancel(true)

            val resultIntent = Intent(this@FCMMessageHandler, WelcomeActivity::class.java)
            resultIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
            val resultPendingIntent = PendingIntent.getActivity(this@FCMMessageHandler, 0, resultIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT)
            builder.setContentIntent(resultPendingIntent)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForeground(NOTIFICATION_ID, builder.build())
            } else {
                val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                service.notify(NOTIFICATION_ID, builder.build())
            }
        }
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(): String {
        val channel = NotificationChannel(GESSEL_CHANNEL,
                GESSEL_CHANNEL, NotificationManager.IMPORTANCE_NONE)
        channel.lightColor = ContextCompat.getColor(this, R.color.colorAccent)
        channel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(channel)
        return GESSEL_CHANNEL
    }

    override fun onDestroy() {
        stopForeground(false)
        super.onDestroy()
    }


    companion object {
        private const val NOTIFICATION_ID = 987
        private const val GESSEL_CHANNEL = "gessel"
    }
}