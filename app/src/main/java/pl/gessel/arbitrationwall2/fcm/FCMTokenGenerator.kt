package pl.gessel.arbitrationwall2.fcm

import android.content.SharedPreferences
import com.google.android.gms.ads.identifier.AdvertisingIdClient
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import dagger.android.AndroidInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.gessel.arbitrationwall2.networking.ArticleService
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject

class FCMTokenGenerator : FirebaseInstanceIdService() {

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    @Inject
    lateinit var articleService: ArticleService

    private val googleAdvertisementId: String?
        get() {
            var adInfo: AdvertisingIdClient.Info? = null
            try {
                adInfo = AdvertisingIdClient.getAdvertisingIdInfo(this@FCMTokenGenerator)
            } catch (exception: IOException) {
                Timber.e(exception.message)
            } catch (exception: GooglePlayServicesRepairableException) {
                Timber.e(exception.message)
            } catch (exception: GooglePlayServicesNotAvailableException) {
                Timber.e(exception.message)
            }

            return adInfo!!.id
        }

    override fun onCreate() {
        AndroidInjection.inject(this)
        super.onCreate()
    }

    override fun onTokenRefresh() {
        val refreshedToken = FirebaseInstanceId.getInstance().token
        var deviceId = googleAdvertisementId
        if (deviceId == null) {
            deviceId = "deviceDoNotHaveAdvertisementId"
        }
        sharedPreferences.edit().putString(DEVICE_ID, deviceId).apply()
        val fcmRegistration = FCMRegistration(refreshedToken, deviceId)
        Timber.i("FCM Token " + fcmRegistration.idRegistration)
        sendFcmToken(fcmRegistration)
    }

    private fun sendFcmToken(fcmRegistration: FCMRegistration) {
        articleService.registerFcmToken(
                ArticleService.GESSEL_APPLICATION,
                ArticleService.REGISTER_DEVICE_ACTION,
                fcmRegistration.idRegistration,
                fcmRegistration.idDevice,
                ArticleService.DEVICE_TYPE,
                NOTIFICATION_ALL)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
    }

    companion object {
        private const val DEVICE_ID = "deviceId"
        private const val NOTIFICATION_ALL = 2
    }
}
